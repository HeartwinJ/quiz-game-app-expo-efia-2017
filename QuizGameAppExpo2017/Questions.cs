﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizGameAppExpo2017
{
    public partial class Questions : MetroFramework.Forms.MetroForm
    {
        StartForm OriginalForm;
        MainForm mainForm;
        //QuestionsClass questionsClass = new QuestionsClass();
        string option = "NULL";

        public Questions(StartForm IncomingForm, MainForm INC_mainForm)
        {
            OriginalForm = IncomingForm;
            mainForm = INC_mainForm;
            InitializeComponent();
        }

        private void Questions_Load(object sender, EventArgs e)
        {
            //Form Load
            //string tempString = questionsClass.username;
            //MessageBox.Show(QuestionsClass.username);
            metroLabel2.Text = "Hello " + QuestionsClass.username;
            QuestionsClass.loadQuestions();
            QuestionsClass.setQuestion(this);

            //WindowState = FormWindowState.Maximized;
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            //Next Button
                       
            if(option == "NULL")
            {
                MessageBox.Show("Please Choose an option!");
                return;
            }
            else if(option != "NULL")
            {
                QuestionsClass.index++;
                //MessageBox.Show(QuestionsClass.index.ToString());
                QuestionsClass.validateQuestion(this, option);
                QuestionsClass.loadQuestions();
                QuestionsClass.setQuestion(this);

                //to reset the radio buttons
                metroRadioButton1.Checked = false;
                metroRadioButton2.Checked = false;
                metroRadioButton3.Checked = false;
                metroRadioButton4.Checked = false;
            }

            ////////////////////////////////////////////////////////////////
            //This number decides the Number of Questions Given per Player//
            ////////////////////////////////////////////////////////////////

            if(QuestionsClass.index > 5)
            {
                SummaryForm summaryForm = new SummaryForm(mainForm);
                summaryForm.Show();
                this.Close();
            }

            //questionsClass.setQuestion(this);
        }

        private void metroRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            option = metroRadioButton1.Text;
        }

        private void metroRadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            option = metroRadioButton2.Text;
        }

        private void metroRadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            option = metroRadioButton3.Text;
        }

        private void metroRadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            option = metroRadioButton4.Text;
        }
    }
}
