﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizGameAppExpo2017
{
    public static class QuestionsClass
    {
        //public QuestionsClass()
        //{
        //Constructor
        //}

        //Random Number Generation
        public static Random random = new Random();

        public static void retRand()
        {
            qindex = random.Next(0, 153);
        }

        //This vvariable controls the assignment of questions
        public static int qindex;

        public static string username;

        //the number here shud be the actual number of questions or 1 + index
        public static string[] myQuestion = new string[154];
        public static string[,] myOptions = new string[154,4];
        private static string[] correct = new string[154];

        public static int score;
        public static int correct_int;
        public static int wrong_int;

        public static int index;

        public static void loadQuestions()
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////                                                                  ///////////////////
            /////////////////           I HAVE 154 QUESTIONS IN THIS PROGRAM                   ///////////////////
            /////////////////                                                                  ///////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////

            ////INDEX = 
            //myQuestion[] = "Q" + index.ToString() + " " + "";
            //myOptions[, 0] = "";
            //myOptions[, 1] = "";
            //myOptions[, 2] = "";
            //myOptions[, 3] = "";
            //correct[] = "";

            //INDEX = 0
            myQuestion[0] = "Q" + index.ToString() + " " + "Grand Central Terminal, Park Avenue, New York is the world's";
            myOptions[0, 0] = "highest railway station";
            myOptions[0, 1] = "longest railway station";
            myOptions[0, 2] = "largest railway station";
            myOptions[0, 3] = "None of the Above";
            correct[0] = "largest railway station";

            //INDEX = 1
            myQuestion[1] = "Q" + index.ToString() + " " + "Entomology is the science that studies";
            myOptions[1, 0] = "The origin and history of technical and scientific terms";
            myOptions[1, 1] = "Insects";
            myOptions[1, 2] = "Behavior of human beings";
            myOptions[1, 3] = "None of the Above";
            correct[1] = "Insects";

            //INDEX = 2
            myQuestion[2] = "Q" + index.ToString() + " " + "Eritrea, which became the 182nd member of the UN in 1993, is in the continent of";
            myOptions[2, 0] = "Africa";
            myOptions[2, 1] = "Australia";
            myOptions[2, 2] = "Europe";
            myOptions[2, 3] = "Asia";
            correct[2] = "Africa";

            //INDEX = 3
            myQuestion[3] = "Q" + index.ToString() + " " + "Garampani sanctuary is located at";
            myOptions[3, 0] = "Gangtok, Sikkim";
            myOptions[3, 1] = "Kohima, Nagaland";
            myOptions[3, 2] = "Diphu, Assam";
            myOptions[3, 3] = "Junagarh, Gujarat";
            correct[3] = "Diphu, Assam";

            //INDEX = 4
            myQuestion[4] = "Q" + index.ToString() + " " + "Hitler party which came into power in 1933 is known as";
            myOptions[4, 0] = "Ku-Klux-Klan";
            myOptions[4, 1] = "Democratic Party";
            myOptions[4, 2] = "Labour Party";
            myOptions[4, 3] = "Nazi Party";
            correct[4] = "Nazi Party";

            //INDEX = 5
            myQuestion[5] = "Q" + index.ToString() + " " + "For which of the following disciplines is Nobel Prize awarded?";
            myOptions[5, 0] = "Literature, Peace and Economics";
            myOptions[5, 1] = "Physics and Chemistry";
            myOptions[5, 2] = "Physiology or Medicine";
            myOptions[5, 3] = "All of the above";
            correct[5] = "All of the above";

            //INDEX = 6
            myQuestion[6] = "Q" + index.ToString() + " " + "FFC stands for";
            myOptions[6, 0] = "Federation of Football Council";
            myOptions[6, 1] = "Film Finance Corporation";
            myOptions[6, 2] = "Foreign Finance Corporation";
            myOptions[6, 3] = "None of the above";
            correct[6] = "Film Finance Corporation";

            //INDEX = 7
            myQuestion[7] = "Q" + index.ToString() + " " + "Fastest shorthand writer was";
            myOptions[7, 0] = "Dr. G. D. Bist";
            myOptions[7, 1] = "J.M. Tagore";
            myOptions[7, 2] = "Khudada Khan";
            myOptions[7, 3] = "J.R.D. Tata";
            correct[7] = "Dr. G. D. Bist";

            //INDEX = 8
            myQuestion[8] = "Q" + index.ToString() + " " + "Epsom (England) is the place associated with";
            myOptions[8, 0] = "Shooting";
            myOptions[8, 1] = "Horse racing";
            myOptions[8, 2] = "Polo";
            myOptions[8, 3] = "Snooker";
            correct[8] = "Horse racing";

            //INDEX = 9
            myQuestion[9] = "Q" + index.ToString() + " " + "First human heart transplant operation conducted by Dr. Christian Bernard on Louis Washkansky, was conducted in";
            myOptions[9, 0] = "1958";
            myOptions[9, 1] = "1922";
            myOptions[9, 2] = "1967";
            myOptions[9, 3] = "1968";
            correct[9] = "1967";

            //INDEX = 10
            myQuestion[10] = "Q" + index.ToString() + " " + "Galileo was an Italian astronomer who";
            myOptions[10, 0] = "developed the telescope";
            myOptions[10, 1] = "discovered four satellites of Jupiter";
            myOptions[10, 2] = "discovered that the movement of pendulum produces a regular time measurement";
            myOptions[10, 3] = "All of the above";
            correct[10] = "All of the above";

            //INDEX = 11
            myQuestion[11] = "Q" + index.ToString() + " " + "Habeas Corpus Act 1679";
            myOptions[11, 0] = "provided facilities to a prisoner to obtain either speedy trial or release in bail ";
            myOptions[11, 1] = "states that no one was to be imprisoned without a writ or warrant stating the charge against him";
            myOptions[11, 2] = "safeguarded the personal liberties of the people against arbitrary imprisonment by the king's orders";
            myOptions[11, 3] = "All of the above";
            correct[11] = "All of the above";

            //INDEX = 12
            myQuestion[12] = "Q" + index.ToString() + " " + "Exposure to sunlight helps a person improve his health because";
            myOptions[12, 0] = "resistance power increases";
            myOptions[12, 1] = "the infrared light kills bacteria in the body";
            myOptions[12, 2] = "the ultraviolet rays convert skin oil into Vitamin D";
            myOptions[12, 3] = "the pigment cells in the skin get stimulated and produce a healthy tan";
            correct[12] = "the ultraviolet rays convert skin oil into Vitamin D";

            //INDEX = 13
            myQuestion[13] = "Q" + index.ToString() + " " + "Golf player Vijay Singh belongs to which country?";
            myOptions[13, 0] = "India";
            myOptions[13, 1] = "Fiji";
            myOptions[13, 2] = "UK";
            myOptions[13, 3] = "USA";
            correct[13] = "Fiji";

            //INDEX = 14
            myQuestion[14] = "Q" + index.ToString() + " " + "Guarantee to an exporter that the importer of his goods will pay immediately for the goods ordered by him, is known as";
            myOptions[14, 0] = "Letter of Credit (L/C)";
            myOptions[14, 1] = "inflation";
            myOptions[14, 2] = "laissezfaire";
            myOptions[14, 3] = "None of the above";
            correct[14] = "Letter of Credit (L/C)";

            //INDEX = 15
            myQuestion[15] = "Q" + index.ToString() + " " + "First Afghan War took place in";
            myOptions[15, 0] = "1839";
            myOptions[15, 1] = "1848";
            myOptions[15, 2] = "1843";
            myOptions[15, 3] = "1833 ";
            correct[15] = "1839";

            //INDEX = 16
            myQuestion[16] = "Q" + index.ToString() + " " + "Gulf cooperation council was originally formed by";
            myOptions[16, 0] = "Bahrain, Kuwait, Oman, Qatar, Saudi Arabia and United Arab Emirates";
            myOptions[16, 1] = "Second World Nations";
            myOptions[16, 2] = "Third World Nations";
            myOptions[16, 3] = "Fourth World Nations";
            correct[16] = "Bahrain, Kuwait, Oman, Qatar, Saudi Arabia and United Arab Emirates";

            //INDEX = 17
            myQuestion[17] = "Q" + index.ToString() + " " + "First China War was fought between";
            myOptions[17, 0] = "China and Egypt";
            myOptions[17, 1] = "China and Greek";
            myOptions[17, 2] = "China and Britain";
            myOptions[17, 3] = "China and France";
            correct[17] = "China and Britain";

            //INDEX = 18
            myQuestion[18] = "Q" + index.ToString() + " " + "Dumping is";
            myOptions[18, 0] = "selling of goods abroad at a price well below the production cost at the home market price";
            myOptions[18, 1] = "the process by which the supply of a manufacture's product remains low in the domestic market, which";
            myOptions[18, 2] = "prohibited by regulations of GATT";
            myOptions[18, 3] = "All of the above";
            correct[18] = "selling of goods abroad at a price well below the production cost at the home market price";

            //INDEX = 19
            myQuestion[19] = "Q" + index.ToString() + " " + "Federation Cup, World Cup, Allywyn International Trophy and Challenge Cup are awarded to winners of";
            myOptions[19, 0] = "Basketball";
            myOptions[19, 1] = "Tennis";
            myOptions[19, 2] = "Cricket";
            myOptions[19, 3] = "Volleyball";
            correct[19] = "Volleyball";

            //INDEX = 20
            myQuestion[20] = "Q" + index.ToString() + " " + "For the Olympics and World Tournaments, the dimensions of basketball court are";
            myOptions[20, 0] = "27 m x 16 m";
            myOptions[20, 1] = "28 m x 15 m";
            myOptions[20, 2] = "26 m x 14 m";
            myOptions[20, 3] = "28 m x 16 m";
            correct[20] = "28 m x 15 m";

            //INDEX = 21
            myQuestion[21] = "Q" + index.ToString() + " " + "Each year World Red Cross and Red Crescent Day is celebrated on";
            myOptions[21, 0] = "June 8";
            myOptions[21, 1] = "June 18";
            myOptions[21, 2] = "May 8";
            myOptions[21, 3] = "May 18";
            correct[21] = "May 8";

            //INDEX = 22
            myQuestion[22] = "Q" + index.ToString() + " " + "Famous sculptures depicting art of love built some time in 950 AD - 1050 AD are";
            myOptions[22, 0] = "Konark Temple";
            myOptions[22, 1] = "Khajuraho temples";
            myOptions[22, 2] = "Mahabalipuram temples";
            myOptions[22, 3] = "Sun temple";
            correct[22] = "Khajuraho temples";

            //INDEX = 23
            myQuestion[23] = "Q" + index.ToString() + " " + "Gravity setting chambers are used in industries to remove";
            myOptions[23, 0] = "NOx";
            myOptions[23, 1] = "SOx";
            myOptions[23, 2] = "CO";
            myOptions[23, 3] = "suspended particulate matter";
            correct[23] = "suspended particulate matter";

            //INDEX = 24
            myQuestion[24] = "Q" + index.ToString() + " " + "Guwahati High Court is the judicature of";
            myOptions[24, 0] = "Assam";
            myOptions[24, 1] = "Nagaland";
            myOptions[24, 2] = "Arunachal Pradesh";
            myOptions[24, 3] = "All of the above";
            correct[24] = "All of the above";

            //INDEX = 25
            myQuestion[25] = "Q" + index.ToString() + " " + "Friction can be reduced by changing from";
            myOptions[25, 0] = "rolling to sliding";
            myOptions[25, 1] = "sliding to rolling";
            myOptions[25, 2] = "dynamic to static";
            myOptions[25, 3] = "potential energy to kinetic energy";
            correct[25] = "sliding to rolling";

            //INDEX = 26
            myQuestion[26] = "Q" + index.ToString() + " " + "During eleventh Antarctic Expedition in Nov. 1991/March 1992 ____ was installed.";
            myOptions[26, 0] = "Second Permanent Station 'Maitree'";
            myOptions[26, 1] = "First permanent station 'Dakshin Gangotri'  ";
            myOptions[26, 2] = "SODAR (Sonic Detection And Ranging)";
            myOptions[26, 3] = "None of the above";
            correct[26] = "SODAR (Sonic Detection And Ranging)";

            //INDEX = 27
            myQuestion[27] = "Q" + index.ToString() + " " + "From the following, choose the set in which names of Secretary-Generals of the UNO are arranged in correct chronological order?";
            myOptions[27, 0] = "Trygve Lie, Dag Hammarskjoeld, U Thant, Kurt Waldheim.";
            myOptions[27, 1] = "U Thant, Dag Hammarskjoeld, Trygve Lie, Kurt Waldheim.";
            myOptions[27, 2] = "U Thant, Kurt Waldheim, Dag Hammarskjoeld, Trygve Lie.";
            myOptions[27, 3] = "Trygve Lie, U Thant , Dag Hammarskjoeld, Kurt Waldheim.";
            correct[27] = "Trygve Lie, Dag Hammarskjoeld, U Thant, Kurt Waldheim.";

            //INDEX = 28
            myQuestion[28] = "Q" + index.ToString() + " " + "Fire temple is the place of worship of which of the following religion?";
            myOptions[28, 0] = "Taoism";
            myOptions[28, 1] = "Shintoism";
            myOptions[28, 2] = "Judaism";
            myOptions[28, 3] = "Zoroastrianism (Parsi Religion)";
            correct[28] = "Zoroastrianism (Parsi Religion)";

            //INDEX = 29
            myQuestion[29] = "Q" + index.ToString() + " " + " Film and TV institute of India is located at";
            myOptions[29, 0] = "Pune (Maharashtra)";
            myOptions[29, 1] = "Pimpri (Maharashtra)";
            myOptions[29, 2] = "Perambur (Tamilnadu)";
            myOptions[29, 3] = "Rajkot (Gujarat)";
            correct[29] = "Pune (Maharashtra)";

            //INDEX = 30
            myQuestion[30] = "Q" + index.ToString() + " " + "Georgia, Uzbekistan and Turkmenistan became the members of UNO in";
            myOptions[30, 0] = "1991";
            myOptions[30, 1] = "1992";
            myOptions[30, 2] = "1993";
            myOptions[30, 3] = "1994 ";
            correct[30] = "1992";

            //INDEX = 31
            myQuestion[31] = "Q" + index.ToString() + " " + "Guru Gobind Singh was";
            myOptions[31, 0] = "the 10th Guru of the Sikhs";
            myOptions[31, 1] = "founder of Khalsa, the inner council of the Sikhs in 1699";
            myOptions[31, 2] = "author of Dasam Granth";
            myOptions[31, 3] = "All the above";
            correct[31] = "All the above";

            //INDEX = 32
            myQuestion[32] = "Q" + index.ToString() + " " + "Hermann Scheer (Germany) received right Livelihood Award in 1999 for";
            myOptions[32, 0] = "his long standing efforts to end the impunity of dictators";
            myOptions[32, 1] = "his indefatigable work for thepromotion of solar energy worldwide";
            myOptions[32, 2] = "showing that organic agriculture is a key to both environmental sustainability and food security";
            myOptions[32, 3] = "None of the above";
            correct[32] = "his indefatigable work for thepromotion of solar energy worldwide";

            //INDEX = 33
            myQuestion[33] = "Q" + index.ToString() + " " + "Germany signed the Armistice Treaty on ____ and World War I ended";
            myOptions[33, 0] = "May 30, 1918";
            myOptions[33, 1] = "February 15, 1918";
            myOptions[33, 2] = "November 11, 1918";
            myOptions[33, 3] = "January 19, 1918";
            correct[33] = "November 11, 1918";

            //INDEX = 34
            myQuestion[34] = "Q" + index.ToString() + " " + "During World War II, when did Germany attack France?";
            myOptions[34, 0] = "1940";
            myOptions[34, 1] = "1941";
            myOptions[34, 2] = "1942";
            myOptions[34, 3] = "1943";
            correct[34] = "1940";

            //INDEX = 35
            myQuestion[35] = "Q" + index.ToString() + " " + "Frederick Sanger is a twice recipient of the Nobel Prize for";
            myOptions[35, 0] = "Physics in 1956 and 1972";
            myOptions[35, 1] = "Chemistry in 1958 and 1980";
            myOptions[35, 2] = "Physics in 1903 and Chemistry in 1911";
            myOptions[35, 3] = "Chemistry in 1954 and Peace in 1962";
            correct[35] = "Chemistry in 1958 and 1980";

            //INDEX = 36
            myQuestion[36] = "Q" + index.ToString() + " " + "The ozone layer restricts";
            myOptions[36, 0] = "X-rays and gamma rays";
            myOptions[36, 1] = "Visible light";
            myOptions[36, 2] = "Infrared radiation";
            myOptions[36, 3] = "Ultraviolet radiation";
            correct[36] = "Ultraviolet radiation";

            //INDEX = 37
            myQuestion[37] = "Q" + index.ToString() + " " + "Eugenics is the study of";
            myOptions[37, 0] = "genetic of plants";
            myOptions[37, 1] = "different races of mankind";
            myOptions[37, 2] = "people of European origin";
            myOptions[37, 3] = "altering human beings by changing their genetic components";
            correct[37] = "altering human beings by changing their genetic components";

            //INDEX = 38
            myQuestion[38] = "Q" + index.ToString() + " " + "Euclid was";
            myOptions[38, 0] = "Greek mathematician";
            myOptions[38, 1] = "Propounded the geometrical theorems";
            myOptions[38, 2] = "Contributor to the use of deductive principles of logic as the basis of geometry";
            myOptions[38, 3] = "All of the above";
            correct[38] = "All of the above";

            //INDEX = 39
            myQuestion[39] = "Q" + index.ToString() + " " + "Ecology deals with";
            myOptions[39, 0] = "Cell formation";
            myOptions[39, 1] = "Birds";
            myOptions[39, 2] = "Relation between organisms and their environment";
            myOptions[39, 3] = "Tissues";
            correct[39] = "Relation between organisms and their environment";

            //INDEX = 40
            myQuestion[40] = "Q" + index.ToString() + " " + "Filaria is caused by";
            myOptions[40, 0] = "Protozoa";
            myOptions[40, 1] = "Mosquito";
            myOptions[40, 2] = "Virus";
            myOptions[40, 3] = "Bacteria";
            correct[40] = "Mosquito";

            //INDEX = 41
            myQuestion[41] = "Q" + index.ToString() + " " + "Goa Shipyard Limited (GSL) was established in";
            myOptions[41, 0] = "1955";
            myOptions[41, 1] = "1956";
            myOptions[41, 2] = "1957";
            myOptions[41, 3] = "1958";
            correct[41] = "1957";

            //INDEX = 42
            myQuestion[42] = "Q" + index.ToString() + " " + "DRDL stands for";
            myOptions[42, 0] = "Defence Research and Development Laboratary";
            myOptions[42, 1] = "Differential Research and Documentation Laboratary";
            myOptions[42, 2] = "Department of Research and Development Laboratory";
            myOptions[42, 3] = "None of the above";
            correct[42] = "Defence Research and Development Laboratary";

            //INDEX = 43
            myQuestion[43] = "Q" + index.ToString() + " " + "Who was the first Indian Chief of Army Staff of the Indian Army ?";
            myOptions[43, 0] = "Gen. K.M. Cariappa";
            myOptions[43, 1] = "Gen. Maharaja Rajendra Singhji";
            myOptions[43, 2] = "Vice-Admiral R.D. Katari";
            myOptions[43, 3] = "None of the above";
            correct[43] = "Gen. K.M. Cariappa";

            //INDEX = 44
            myQuestion[44] = "Q" + index.ToString() + " " + "FRS stands for";
            myOptions[44, 0] = "Federation of Regulation Society";
            myOptions[44, 1] = "Fellow Research System";
            myOptions[44, 2] = "Fellow of Royal Society";
            myOptions[44, 3] = "None of the above";
            correct[44] = "Fellow of Royal Society";

            //INDEX = 45
            myQuestion[45] = "Q" + index.ToString() + " " + "Escape velocity of a rocket fired from the earth towards the moon is a velocity to get rid of the";
            myOptions[45, 0] = "Centripetal force due to the earth's rotation   ";
            myOptions[45, 1] = "Earth's gravitational pull";
            myOptions[45, 2] = "Pressure of the atmosphere";
            myOptions[45, 3] = "Moon's gravitational pull";
            correct[45] = "Earth's gravitational pull";

            //INDEX = 46
            myQuestion[46] = "Q" + index.ToString() + " " + "GATT (General Agreement on Tariffs and Trade) is";
            myOptions[46, 0] = "international agreement signed in 1947 between non-communist nations with the object of encouraging";
            myOptions[46, 1] = "agreement which seeks to achieve its aim by arranging and encouraging bargaining with trade concessi";
            myOptions[46, 2] = "Both option A and B";
            myOptions[46, 3] = "None of the above";
            correct[46] = "Both option A and B";

            //INDEX = 47
            myQuestion[47] = "Q" + index.ToString() + " " + "Coral reefs in India can be found in";
            myOptions[47, 0] = "Waltair";
            myOptions[47, 1] = "Trivandrum";
            myOptions[47, 2] = "the coast of Orissa";
            myOptions[47, 3] = "Rameshwaram";
            correct[47] = "Rameshwaram";

            //INDEX = 48
            myQuestion[48] = "Q" + index.ToString() + " " + "For safety, the fuse wire used in the mains for household supply of electricity must be made of metal having";
            myOptions[48, 0] = "low specific heat";
            myOptions[48, 1] = "high melting point";
            myOptions[48, 2] = "high resistance";
            myOptions[48, 3] = "low melting point";
            correct[48] = "low melting point";

            //INDEX = 49
            myQuestion[49] = "Q" + index.ToString() + " " + "Golden Temple, Amritsar is India's";
            myOptions[49, 0] = "largest Gurdwara";
            myOptions[49, 1] = "oldest Gurudwara";
            myOptions[49, 2] = "Both option A and B are correct";
            myOptions[49, 3] = "None of the above";
            correct[49] = "largest Gurdwara";

            //INDEX = 50
            myQuestion[50] = "Q" + index.ToString() + " " + "During World War I Germany was defeated in the Battle of Verdun on the western front and Romania declared war on the eastern front in the year";
            myOptions[50, 0] = "1914 AD";
            myOptions[50, 1] = "1915 AD";
            myOptions[50, 2] = "1916 AD";
            myOptions[50, 3] = "1917 AD";
            correct[50] = "1916 AD";

            //INDEX = 51
            myQuestion[51] = "Q" + index.ToString() + " " + "Heavy Water Project (Talcher) and Fertilizer plant (Paradeep) are famous industries of";
            myOptions[51, 0] = "Orissa";
            myOptions[51, 1] = "Kerala";
            myOptions[51, 2] = "Tamil nadu";
            myOptions[51, 3] = "Andhra Pradesh";
            correct[51] = "Orissa";

            //INDEX = 52
            myQuestion[52] = "Q" + index.ToString() + " " + "Hamid Karzai was chosen president of Afghanistan in";
            myOptions[52, 0] = "2000";
            myOptions[52, 1] = "2001";
            myOptions[52, 2] = "2002";
            myOptions[52, 3] = "2003";
            correct[52] = "2002";

            //INDEX = 53
            myQuestion[53] = "Q" + index.ToString() + " " + " Durand Cup is associated with the game of";
            myOptions[53, 0] = "Volleyball";
            myOptions[53, 1] = "Football";
            myOptions[53, 2] = "Cricket";
            myOptions[53, 3] = "Hockey";
            correct[53] = "Football";

            //INDEX = 54
            myQuestion[54] = "Q" + index.ToString() + " " + "Headquarters of UNO is situated in";
            myOptions[54, 0] = "Paris";
            myOptions[54, 1] = "Geneva";
            myOptions[54, 2] = "Haque (Netherlands)";
            myOptions[54, 3] = "New York, USA";
            correct[54] = "New York, USA";

            //INDEX = 55
            myQuestion[55] = "Q" + index.ToString() + " " + "First International Peace Congress was held in London in";
            myOptions[55, 0] = "1564 AD";
            myOptions[55, 1] = "1798 AD";
            myOptions[55, 2] = "1843 AD";
            myOptions[55, 3] = "1901 AD";
            correct[55] = "1843 AD";
         
            //INDEX = 56
            myQuestion[56] = "Q" + index.ToString() + " " + "For seeing objects at the surface of water from a submarine under water, the instrument used is";
            myOptions[56, 0] = "periscope";
            myOptions[56, 1] = "telescope";
            myOptions[56, 2] = "kaleidoscope";
            myOptions[56, 3] = "spectroscope";
            correct[56] = "periscope";

            //INDEX = 57
            myQuestion[57] = "Q" + index.ToString() + " " + "Dr. Zakir Hussain was";
            myOptions[57, 0] = "first vice president of India";
            myOptions[57, 1] = "first speaker of Lok Sabha";
            myOptions[57, 2] = "the first Muslim president of India";
            myOptions[57, 3] = "first president of Indian National Congress";
            correct[57] = "the first Muslim president of India";

            //INDEX = 58
            myQuestion[58] = "Q" + index.ToString() + " " + "G-15 is an economic grouping of";
            myOptions[58, 0] = "First World Nations";
            myOptions[58, 1] = "Second World Nations";
            myOptions[58, 2] = "Third World Nations";
            myOptions[58, 3] = "Fourth World Nations";
            correct[58] = "Third World Nations";

            //INDEX = 59
            myQuestion[59] = "Q" + index.ToString() + " " + "Fathometer is used to measure";
            myOptions[59, 0] = "Rainfall";
            myOptions[59, 1] = "Ocean depth";
            myOptions[59, 2] = "Sound intensity";
            myOptions[59, 3] = "Earthquakes";
            correct[59] = "";

            //INDEX = 60
            myQuestion[60] = "Q" + index.ToString() + " " + "For galvanizing iron which of the following metals is used?";
            myOptions[60, 0] = "Lead";
            myOptions[60, 1] = "Aluminium";
            myOptions[60, 2] = "Copper";
            myOptions[60, 3] = "Zinc";
            correct[60] = "Zinc";

            //INDEX = 61
            myQuestion[61] = "Q" + index.ToString() + " " + "Economic goods are";
            myOptions[61, 0] = "Commodities that is available according to their demand";
            myOptions[61, 1] = "Commodities that is available more as compared to demand";
            myOptions[61, 2] = "all commodities that are limited in quantity as compared to their demand";
            myOptions[61, 3] = "None of the above";
            correct[61] = "all commodities that are limited in quantity as compared to their demand";

            //INDEX = 62
            myQuestion[62] = "Q" + index.ToString() + " " + "For purifying drinking water alum is used";
            myOptions[62, 0] = "for coagulation of mud particles";
            myOptions[62, 1] = "to remove gases";
            myOptions[62, 2] = "to kill bacteria";
            myOptions[62, 3] = "to remove salts";
            correct[62] = "for coagulation of mud particles";

            //INDEX = 63
            myQuestion[63] = "Q" + index.ToString() + " " + "Hockey was introduced in the Asian Games in";
            myOptions[63, 0] = "1966 in Bangkok";
            myOptions[63, 1] = "1958 in Tokyo";
            myOptions[63, 2] = "1962 in Jakarta";
            myOptions[63, 3] = "1970 in Bangkok";
            correct[63] = "1958 in Tokyo";

            //INDEX = 64
            myQuestion[64] = "Q" + index.ToString() + " " + "ESCAP stands for";
            myOptions[64, 0] = "European Society Council for Africa and Pacific";
            myOptions[64, 1] = "Economic and Social Commission for Asia and Pacific";
            myOptions[64, 2] = "Economic and Social Commitee for Africa and Pacific";
            myOptions[64, 3] = "None of the above";
            correct[64] = "Economic and Social Commission for Asia and Pacific";

            //INDEX = 65
            myQuestion[65] = "Q" + index.ToString() + " " + "Firdausi was";
            myOptions[65, 0] = "a poet";
            myOptions[65, 1] = "well known for his epic 'Shahnama'";
            myOptions[65, 2] = "Both option A and B";
            myOptions[65, 3] = "None of the above";
            correct[65] = "Both option A and B";

            //INDEX = 66
            myQuestion[66] = "Q" + index.ToString() + " " + "Himalayan Mountaineering Institute is at";
            myOptions[66, 0] = "Dispur";
            myOptions[66, 1] = "Marmago";
            myOptions[66, 2] = "Dehradun";
            myOptions[66, 3] = "Darjeeling";
            correct[66] = "Darjeeling";

            //INDEX = 67
            myQuestion[67] = "Q" + index.ToString() + " " + "Gabriel Daniel Fahrenheit";
            myOptions[67, 0] = "was a German Physicist";
            myOptions[67, 1] = "devised temperature scale";
            myOptions[67, 2] = "developed the mercury thermometer in 1714";
            myOptions[67, 3] = "All of the above";
            correct[67] = "All of the above";

            //INDEX = 68
            myQuestion[68] = "Q" + index.ToString() + " " + "During the first crusade, crusaders reached Jerusalem and captured it in";
            myOptions[68, 0] = "1000 AD";
            myOptions[68, 1] = "1099 AD";
            myOptions[68, 2] = "1200 AD";
            myOptions[68, 3] = "1515 AD";
            correct[68] = "1099 AD";

            //INDEX = 69
            myQuestion[69] = "Q" + index.ToString() + " " + "Dr. Linus Carl Pauling is the only person to have won two Nobel prizes individually for";
            myOptions[69, 0] = "Peace Prize in 1954, Chemistry in 1962";
            myOptions[69, 1] = "Medicine in 1954, Physics in 1962";
            myOptions[69, 2] = "Chemistry in 1954, Peace Prize in 1962";
            myOptions[69, 3] = "Physics in 1954, Medicine in 1962";
            correct[69] = "Chemistry in 1954, Peace Prize in 1962";

            //INDEX = 70
            myQuestion[70] = "Q" + index.ToString() + " " + "What is the moto of the National Cadet Corps (NCC)?";
            myOptions[70, 0] = "Veerta aur Vivek";
            myOptions[70, 1] = "Bharat Mata Ki Jai";
            myOptions[70, 2] = "Vayam Rakshaamah";
            myOptions[70, 3] = "Ekta aur Anushasan";
            correct[70] = "Ekta aur Anushasan";

            //INDEX = 71
            myQuestion[71] = "Q" + index.ToString() + " " + "Grey Revolution is associated with:";
            myOptions[71, 0] = "Orange";
            myOptions[71, 1] = "Sand";
            myOptions[71, 2] = "Wool";
            myOptions[71, 3] = "Coal";
            correct[71] = "Wool";

            //INDEX = 72
            myQuestion[72] = "Q" + index.ToString() + " " + "Which of the following Gallantry award was known as Ashok Chakra, class II before 1967?";
            myOptions[72, 0] = "Saurya Chakra";
            myOptions[72, 1] = "Kirti Chakra";
            myOptions[72, 2] = "Param Veer Chakra";
            myOptions[72, 3] = "Veer Chakra";
            correct[72] = "Kirti Chakra";

            //INDEX = 73
            myQuestion[73] = "Q" + index.ToString() + " " + "Which country, on the map of world, appears as 'Long Shoe'?";
            myOptions[73, 0] = "Portugal";
            myOptions[73, 1] = "Italy";
            myOptions[73, 2] = "Greece";
            myOptions[73, 3] = "Hungary";
            correct[73] = "Italy";

            //INDEX = 74
            myQuestion[74] = "Q" + index.ToString() + " " + "The motto of UNO is :";
            myOptions[74, 0] = "It's your world!";
            myOptions[74, 1] = "Life for All!";
            myOptions[74, 2] = "Peace!";
            myOptions[74, 3] = "Love and Peace!";
            correct[74] = "It's your world!";

            //INDEX = 75
            myQuestion[75] = "Q" + index.ToString() + " " + "Which from the following countries do NOT yeild veto-power?";
            myOptions[75, 0] = "United States";
            myOptions[75, 1] = "United Kingdom";
            myOptions[75, 2] = "Canada";
            myOptions[75, 3] = "France";
            correct[75] = "Canada";

            //INDEX = 76
            myQuestion[76] = "Q" + index.ToString() + " " + "Which company is nicknamed as 'Big Blue'?";
            myOptions[76, 0] = "IBM";
            myOptions[76, 1] = "Microsoft";
            myOptions[76, 2] = "Apple";
            myOptions[76, 3] = "Micromax";
            correct[76] = "IBM";

            //INDEX = 77
            myQuestion[77] = "Q" + index.ToString() + " " + "What is the function of a dynamo?";
            myOptions[77, 0] = "To convert heat energy into light energy   ";
            myOptions[77, 1] = "To convert light energy into heat energy";
            myOptions[77, 2] = "To convert mechanical energy into electrical energy";
            myOptions[77, 3] = "To convert electrical energy into mechanical energy";
            correct[77] = "To convert mechanical energy into electrical energy";

            //INDEX = 78
            myQuestion[78] = "Q" + index.ToString() + " " + "What is the population density of Manipur?";
            myOptions[78, 0] = "107/sq. km";
            myOptions[78, 1] = "207/sq. km";
            myOptions[78, 2] = "307/sq. km";
            myOptions[78, 3] = "407/sq. km";
            correct[78] = "107/sq. km";

            //INDEX = 79
            myQuestion[79] = "Q" + index.ToString() + " " + "Yuri Gagarin was";
            myOptions[79, 0] = "a Russian cosmonaut & became the first man to travel in space.";
            myOptions[79, 1] = "the first person to fly at great height of about 340 km from the earth";
            myOptions[79, 2] = "both (a) and (b)";
            myOptions[79, 3] = "None of the above";
            correct[79] = "both (a) and (b)";

            //INDEX = 80
            myQuestion[80] = "Q" + index.ToString() + " " + "When did Commander Robert Peary discover the North Pole?";
            myOptions[80, 0] = "1904";
            myOptions[80, 1] = "1905";
            myOptions[80, 2] = "1908";
            myOptions[80, 3] = "1909";
            correct[80] = "1909";

            //INDEX = 81
            myQuestion[81] = "Q" + index.ToString() + " " + "When did the Indonesian settlement in the Malay Peninsula take place?";
            myOptions[81, 0] = "2400 BC";
            myOptions[81, 1] = "2300 BC";
            myOptions[81, 2] = "2200 BC";
            myOptions[81, 3] = "2100 BC";
            correct[81] = "2200 BC";

            //INDEX = 82
            myQuestion[82] = "Q" + index.ToString() + " " + "When did the US solar system probe Voyager-two discover six new moons of the planet Uranus?";
            myOptions[82, 0] = "1986";
            myOptions[82, 1] = "1916";
            myOptions[82, 2] = "1946";
            myOptions[82, 3] = "1966";
            correct[82] = "1986";

            //INDEX = 83
            myQuestion[83] = "Q" + index.ToString() + " " + "What is the approximate area of Bihar?";
            myOptions[83, 0] = "94,163 sq. km";
            myOptions[83, 1] = "1,35,100 sq. km";
            myOptions[83, 2] = "3,702 sq. km";
            myOptions[83, 3] = "q1,96,024 sq. km";
            correct[83] = "94,163 sq. km";

            //INDEX = 84
            myQuestion[84] = "Q" + index.ToString() + " " + "When did India host the Common wealth meeting?";
            myOptions[84, 0] = "1961 at Bangalore";
            myOptions[84, 1] = "1976 at Pune";
            myOptions[84, 2] = "1983 at New Delhi";
            myOptions[84, 3] = "None of the above";
            correct[84] = "1983 at New Delhi";

            //INDEX = 85
            myQuestion[85] = "Q" + index.ToString() + " " + "When did China explode the first atomic device?";
            myOptions[85, 0] = "1962";
            myOptions[85, 1] = "1963";
            myOptions[85, 2] = "1964";
            myOptions[85, 3] = "1965";
            correct[85] = "1964";

            //INDEX = 86
            myQuestion[86] = "Q" + index.ToString() + " " + "What is the population density of Nagaland?";
            myOptions[86, 0] = "120/sq. km";
            myOptions[86, 1] = "220/sq. km";
            myOptions[86, 2] = "320/sq. km";
            myOptions[86, 3] = "420/sq. km";
            correct[86] = "120/sq. km";

            //INDEX = 87
            myQuestion[87] = "Q" + index.ToString() + " " + " When did Lal Bahadur Shastri pass away?";
            myOptions[87, 0] = "January, 11";
            myOptions[87, 1] = "January, 20";
            myOptions[87, 2] = "January, 27";
            myOptions[87, 3] = "January, 31";
            correct[87] = "January, 11";

            //INDEX = 88
            myQuestion[88] = "Q" + index.ToString() + " " + "When and where was basketball introduced in Olympics as a medal event?";
            myOptions[88, 0] = "1992 at Barcilona";
            myOptions[88, 1] = "1928 at Paris";
            myOptions[88, 2] = "1936 at Berlin";
            myOptions[88, 3] = "1900 at Athens";
            correct[88] = "1936 at Berlin";

            //INDEX = 89
            myQuestion[89] = "Q" + index.ToString() + " " + "When was the first test tube baby - Louise Brown born?";
            myOptions[89, 0] = "1939";
            myOptions[89, 1] = "1958";
            myOptions[89, 2] = "1981";
            myOptions[89, 3] = "1978";
            correct[89] = "1978";

            //INDEX = 90
            myQuestion[90] = "Q" + index.ToString() + " " + "What is the density of population in Chandigarh?";
            myOptions[90, 0] = "7,902/sq. km";
            myOptions[90, 1] = "1,008/sq. km";
            myOptions[90, 2] = "5,512/sq. km";
            myOptions[90, 3] = "2,669/sq. km";
            correct[90] = "7,902/sq. km";

            //INDEX = 91
            myQuestion[91] = "Q" + index.ToString() + " " + "When cream is separated from milk";
            myOptions[91, 0] = "the density of milk increases";
            myOptions[91, 1] = "the density of milk decreases";
            myOptions[91, 2] = "the density of milk remains unchanged";
            myOptions[91, 3] = "it becomes more viscous";
            correct[91] = "the density of milk increases";

            //INDEX = 92
            myQuestion[92] = "Q" + index.ToString() + " " + "Name the seventh planet from the sun ";
            myOptions[92, 0] = "earth";
            myOptions[92, 1] = "Saturn";
            myOptions[92, 2] = "Uranus";
            myOptions[92, 3] = "Jupiter";
            correct[92] = "Uranus";

            //INDEX = 93
            myQuestion[93] = "Q" + index.ToString() + " " + "Who invented the rabies vaccination? ";
            myOptions[93, 0] = "Louis Pasteur";
            myOptions[93, 1] = "Nancy Snyderman";
            myOptions[93, 2] = "Sir Jonathan Wolfe Miller";
            myOptions[93, 3] = "Peter Larkins";
            correct[93] = "Louis Pasteur";

            //INDEX = 94
            myQuestion[94] = "Q" + index.ToString() + " " + "Each year World Red Cross and Red Crescent Day is celebrated on";
            myOptions[94, 0] = "May 8";
            myOptions[94, 1] = "May 18";
            myOptions[94, 2] = "June 8";
            myOptions[94, 3] = "June 18";
            correct[94] = "May 8";

            //INDEX = 95
            myQuestion[95] = "Q" + index.ToString() + " " + "Germany signed the Armistice Treaty on ____ and World War I ended";
            myOptions[95, 0] = "January 19, 1918";
            myOptions[95, 1] = "May 30, 1918";
            myOptions[95, 2] = "November 11, 1918";
            myOptions[95, 3] = "February 15, 1918";
            correct[95] = "November 11, 1918";

            //INDEX = 96
            myQuestion[96] = "Q" + index.ToString() + " " + "Frederick Sanger is a twice recipient of the Nobel Prize for";
            myOptions[96, 0] = "Chemistry in 1958 and 1980";
            myOptions[96, 1] = "Physics in 1956 and 1972";
            myOptions[96, 2] = "Chemistry in 1954 and Peace in 1962";
            myOptions[96, 3] = "Physics in 1903 and Chemistry in 1911";
            correct[96] = "Chemistry in 1958 and 1980";

            //INDEX = 97
            myQuestion[97] = "Q" + index.ToString() + " " + "Who is the voice actor of Optimus Prime?";
            myOptions[97, 0] = "Brad Pitt";
            myOptions[97, 1] = "Peter Culen";
            myOptions[97, 2] = "Daniel Craig";
            myOptions[97, 3] = "None of the above";
            correct[97] = "Peter Culen";

            //INDEX = 98
            myQuestion[98] = "Q" + index.ToString() + " " + "What was Walt Disney's Mickey Mouse's original name?";
            myOptions[98, 0] = "Manville";
            myOptions[98, 1] = "Melvin";
            myOptions[98, 2] = "Mortimer";
            myOptions[98, 3] = "Murgatroyd";
            correct[98] = "Mortimer";

            //INDEX = 99
            myQuestion[99] = "Q" + index.ToString() + " " + "India became a member of the United Nations in";
            myOptions[99, 0] = "1945";
            myOptions[99, 1] = "1947";
            myOptions[99, 2] = "1959";
            myOptions[99, 3] = "1960";
            correct[99] = "1945";

            //INDEX = 100
            myQuestion[100] = "Q" + index.ToString() + " " + "If the plane of the earth's equator were not inclined to the plane of the earth's orbit";
            myOptions[100, 0] = "the year would be longer";
            myOptions[100, 1] = "the winters would be longer";
            myOptions[100, 2] = "there would be no change of seasons";
            myOptions[100, 3] = "the summers would be warmer";
            correct[100] = "there would be no change of seasons";

            //INDEX = 101
            myQuestion[101] = "Q" + index.ToString() + " " + "When was the International Monetary Fund established?";
            myOptions[101, 0] = "1945";
            myOptions[101, 1] = "1946";
            myOptions[101, 2] = "1947";
            myOptions[101, 3] = "1950";
            correct[101] = "1945";

            //INDEX = 102
            myQuestion[102] = "Q" + index.ToString() + " " + "When was Shakespeare born?";
            myOptions[102, 0] = "1564 AD";
            myOptions[102, 1] = "1618 AD";
            myOptions[102, 2] = "1642 AD";
            myOptions[102, 3] = "1776 AD";
            correct[102] = "1564 AD";

            //INDEX = 
            myQuestion[103] = "Q" + index.ToString() + " " + "Who starred as the vulture in spiderman homecoming?";
            myOptions[103, 0] = "Tom Holland";
            myOptions[103, 1] = "Robert Downey Jr";
            myOptions[103, 2] = "Zendaya";
            myOptions[103, 3] = "Michaell Keaton";
            correct[103] = "Michaell Keaton";

            //INDEX = 
            myQuestion[104] = "Q" + index.ToString() + " " + "Which amongst the following has the lowest metabolic rate of oxygen consumption (mm3/g hour)?";
            myOptions[104, 0] = "Dog";
            myOptions[104, 1] = "Elephant";
            myOptions[104, 2] = "Horse";
            myOptions[104, 3] = "Man";
            correct[104] = "Elephant";

            //INDEX = 105
            myQuestion[105] = "Q" + index.ToString() + " " + "Where did last world cup soccer tournament take place in 2010?";
            myOptions[105, 0] = "Japan and South Korea";
            myOptions[105, 1] = "France";
            myOptions[105, 2] = "South Africa";
            myOptions[105, 3] = "West Germany";
            correct[105] = "South Africa";

            //INDEX = 
            myQuestion[106] = "Q" + index.ToString() + " " + "Where did the three leaders, F.D. Roosevelt, Winston Churchill and Joseph Stalin, meet in 1943 and agreed on the need for maintaining international peace?";
            myOptions[106, 0] = "Moscow";
            myOptions[106, 1] = "San Francisco";
            myOptions[106, 2] = "Teheran";
            myOptions[106, 3] = "Washington D.C.";
            correct[106] = "Teheran";

            //INDEX = 107
            myQuestion[107] = "Q" + index.ToString() + " " + "Where was the headquarters of European Union located?";
            myOptions[107, 0] = "Brussels";
            myOptions[107, 1] = "Paris";
            myOptions[107, 2] = "London";
            myOptions[107, 3] = "Rome";
            correct[107] = "Brussels";

            //INDEX = 108
            myQuestion[108] = "Q" + index.ToString() + " " + "Which of the following are the members of OPEC (Organisation of Petroleum Exporting Countries)?";
            myOptions[108, 0] = "Algeria, Indonesia, Iran, Iraq, Kuwait";
            myOptions[108, 1] = "Libya, United Arab Emirates, Nigeria";
            myOptions[108, 2] = "Qatar, Saudi Arabia, Venezuela";
            myOptions[108, 3] = "All of the above";
            correct[108] = "All of the above";

            //INDEX = 109
            myQuestion[109] = "Q" + index.ToString() + " " + "Which of the following chemicals is useful in photography?";
            myOptions[109, 0] = "Aluminium hydroxide";
            myOptions[109, 1] = "Potassium nitrate";
            myOptions[109, 2] = "Silver bromide";
            myOptions[109, 3] = "Sodium chloride";
            correct[109] = "Silver bromide";

            //INDEX = 110
            myQuestion[110] = "Q" + index.ToString() + " " + "Which are the important meeting of the Commonwealth?";
            myOptions[110, 0] = "Biennial meeting of the Commonwealth Heads of Government (CHOGM)";
            myOptions[110, 1] = "Annual meeting of the finance minister of the member countries";
            myOptions[110, 2] = "Regular meeting of the minister of education, law, health and other minister";
            myOptions[110, 3] = "All of the above";
            correct[110] = "All of the above";

            //INDEX = 111
            myQuestion[111] = "Q" + index.ToString() + " " + "When was slavery abolished in Britain?";
            myOptions[111, 0] = "1830";
            myOptions[111, 1] = "1837";
            myOptions[111, 2] = "1843";
            myOptions[111, 3] = "1833";
            correct[111] = "1833";

            //INDEX = 112
            myQuestion[112] = "Q" + index.ToString() + " " + "Which atomic reactor is used for studies of uranium heavy water lattice?";
            myOptions[112, 0] = "Apsara";
            myOptions[112, 1] = "Zerlina";
            myOptions[112, 2] = "Dhruva";
            myOptions[112, 3] = "Purnima-I";
            correct[112] = "Zerlina";

            //INDEX = 113
            myQuestion[113] = "Q" + index.ToString() + " " + "Which of the following are the member countries of the commonwealth?";
            myOptions[113, 0] = "Australia, Tonga, UK and Zimbabwe";
            myOptions[113, 1] = "Nigeria, Pakistan, India, Jamaica and Singapore";
            myOptions[113, 2] = "Mauritius, Maldives, Ghana, Bangladesh";
            myOptions[113, 3] = "All of the above";
            correct[113] = "All of the above";

            //INDEX = 114
            myQuestion[114] = "Q" + index.ToString() + " " + "When was Mona Lisa painted by Leonardo da Vinci?";
            myOptions[114, 0] = "1431 AD";
            myOptions[114, 1] = "1492 AD";
            myOptions[114, 2] = "1504 AD";
            myOptions[114, 3] = "1556 AD";
            correct[114] = "1504 AD";

            //INDEX = 115
            myQuestion[115] = "Q" + index.ToString() + " " + "Which of the following agencies related to the United Nation was in existence before the Second World War?";
            myOptions[115, 0] = "Food and Agricultural Organisation";
            myOptions[115, 1] = "International Labour Organisation";
            myOptions[115, 2] = "World Health Organisation";
            myOptions[115, 3] = "International Monetary Fund";
            correct[115] = "International Labour Organisation";

            //INDEX = 116
            myQuestion[116] = "Q" + index.ToString() + " " + "Which is the state with largest urban population?";
            myOptions[116, 0] = "West Bengal";
            myOptions[116, 1] = "Maharashtra";
            myOptions[116, 2] = "Kerala";
            myOptions[116, 3] = "Goa";
            correct[116] = "Maharashtra";

            //INDEX = 117
            myQuestion[117] = "Q" + index.ToString() + " " + "Which Bank has the maximum number of branches?";
            myOptions[117, 0] = "ICICI Bank";
            myOptions[117, 1] = "HDFC Bank";
            myOptions[117, 2] = "State Bank of India";
            myOptions[117, 3] = "Axis Bank";
            correct[117] = "State Bank of India";

            //INDEX = 118
            myQuestion[118] = "Q" + index.ToString() + " " + "When was table tennis introduced in Olympics?";
            myOptions[118, 0] = "1896 at Athens";
            myOptions[118, 1] = "1988 at Seoul";
            myOptions[118, 2] = "1924 at Paris";
            myOptions[118, 3] = "1924 at Seoul";
            correct[118] = "1988 at Seoul";

            //INDEX = 119
            myQuestion[119] = "Q" + index.ToString() + " " + "Which is the associated sport of Bombay Gold Cup?";
            myOptions[119, 0] = "Basketball";
            myOptions[119, 1] = "Weightlifting";
            myOptions[119, 2] = "Hockey";
            myOptions[119, 3] = "Football";
            correct[119] = "Hockey";

            //INDEX = 120
            myQuestion[120] = "Q" + index.ToString() + " " + "Which is the place of worship for Judoists?";
            myOptions[120, 0] = "Synagogue";
            myOptions[120, 1] = "First temple";
            myOptions[120, 2] = "No church or temple";
            myOptions[120, 3] = "Monastery";
            correct[120] = "Synagogue";

            //INDEX = 121
            myQuestion[121] = "Q" + index.ToString() + " " + "Which is the sacred text of Hinduism?";
            myOptions[121, 0] = "The Vedas";
            myOptions[121, 1] = "The Bhagavad Gita";
            myOptions[121, 2] = "The epics of the Mahabharata and the Ramayana";
            myOptions[121, 3] = "All of the above";
            correct[121] = "All of the above";

            //INDEX = 122
            myQuestion[122] = "Q" + index.ToString() + " " + "Which amongst the following mammals has the highest metabolic rate in terms of oxygen consumption (mm3/g hour)?";
            myOptions[122, 0] = "Dog";
            myOptions[122, 1] = "Mouse";
            myOptions[122, 2] = "Rabbit";
            myOptions[122, 3] = "Rat";
            correct[122] = "Mouse";

            //INDEX = 123
            myQuestion[123] = "Q" + index.ToString() + " " + "Which among the sources of energy tapped in India has shown the largest growth till the Eighth plan?";
            myOptions[123, 0] = "Hydro";
            myOptions[123, 1] = "Thermal";
            myOptions[123, 2] = "Gas";
            myOptions[123, 3] = "Nuclear";
            correct[123] = "Thermal";

            //INDEX = 124
            myQuestion[124] = "Q" + index.ToString() + " " + "Which is the highest literary award of the world?";
            myOptions[124, 0] = "Nobel Prize";
            myOptions[124, 1] = "Booker Prize";
            myOptions[124, 2] = "Pulitzer Prize";
            myOptions[124, 3] = "Magsaysay Award";
            correct[124] = "Booker Prize";

            //INDEX = 125
            myQuestion[125] = "Q" + index.ToString() + " " + "When light passes from air into glass it experiences change of";
            myOptions[125, 0] = "frequency and wavelength";
            myOptions[125, 1] = "frequency and speed";
            myOptions[125, 2] = "wavelength and speed";
            myOptions[125, 3] = "frequency, wavelength and speed";
            correct[125] = "wavelength and speed";

            //INDEX = 126
            myQuestion[126] = "Q" + index.ToString() + " " + "When is the World's Diabetes Day?";
            myOptions[126, 0] = "14th November";
            myOptions[126, 1] = "11th December";
            myOptions[126, 2] = "15th October";
            myOptions[126, 3] = "1st July";
            correct[126] = "14th November";

            //INDEX = 127
            myQuestion[127] = "Q" + index.ToString() + " " + "When did Margaret Thatcher became the first women Prime Minister of Britain?";
            myOptions[127, 0] = "1998";
            myOptions[127, 1] = "1989";
            myOptions[127, 2] = "1979";
            myOptions[127, 3] = "1800";
            correct[127] = "1979";

            //INDEX = 128
            myQuestion[128] = "Q" + index.ToString() + " " + "When is the International Workers' Day?";
            myOptions[128, 0] = "15th April";
            myOptions[128, 1] = "12th December";
            myOptions[128, 2] = "1st May";
            myOptions[128, 3] = "1st August";
            correct[128] = "1st May";

            //INDEX = 129
            myQuestion[129] = "Q" + index.ToString() + " " + "When a moving bus stops suddenly, the passenger are pushed forward because of the";
            myOptions[129, 0] = "friction between the earth and the bus";
            myOptions[129, 1] = "friction between the passengers and the earth";
            myOptions[129, 2] = "inertia of the passengers";
            myOptions[129, 3] = "inertia of the bus";
            correct[129] = "inertia of the passengers";

            //INDEX = 130
            myQuestion[130] = "Q" + index.ToString() + " " + "When the batsman, in cricket, is out without scoring a single run, is called";
            myOptions[130, 0] = "drive";
            myOptions[130, 1] = "duck";
            myOptions[130, 2] = "flight";
            myOptions[130, 3] = "googly";
            correct[130] = "duck";

            //INDEX = 131
            myQuestion[131] = "Q" + index.ToString() + " " + "When does Russia celebrates its Independence Day?";
            myOptions[131, 0] = "14th November";
            myOptions[131, 1] = "8th October";
            myOptions[131, 2] = "9th August";
            myOptions[131, 3] = "12th june";
            correct[131] = "12th june";

            //INDEX = 132
            myQuestion[132] = "Q" + index.ToString() + " " + "When and where was hockey introduced for women in Olympics?";
            myOptions[132, 0] = "1908 at London";
            myOptions[132, 1] = "1980 at Moscow";
            myOptions[132, 2] = "1936 at Berlin";
            myOptions[132, 3] = "1924 at Paris";
            correct[132] = "1980 at Moscow";

            //INDEX = 133
            myQuestion[133] = "Q" + index.ToString() + " " + "When did Afghanistan ends monarchy and became a republic?";
            myOptions[133, 0] = "1949";
            myOptions[133, 1] = "1973";
            myOptions[133, 2] = "1965";
            myOptions[133, 3] = "2000";
            correct[133] = "1973";

            //INDEX = 134
            myQuestion[134] = "Q" + index.ToString() + " " + "What is the purpose of SAARC?";
            myOptions[134, 0] = "To promote the welfare of the people of South Asia";
            myOptions[134, 1] = "To improve the environment of security in the region";
            myOptions[134, 2] = "To accelerate economic growth and cultural development";
            myOptions[134, 3] = "All of the above";
            correct[134] = "All of the above";

            //INDEX = 135
            myQuestion[135] = "Q" + index.ToString() + " " + "When did Yuri Alekseyevich Gagaris of Russia, the first man to reach space, reached space?";
            myOptions[135, 0] = "1960";
            myOptions[135, 1] = "1961";
            myOptions[135, 2] = "1962";
            myOptions[135, 3] = "1963";
            correct[135] = "1961";

            //INDEX = 136
            myQuestion[136] = "Q" + index.ToString() + " " + "When a given amount of air is cooled";
            myOptions[136, 0] = "the amount of moisture it can hold decreases";
            myOptions[136, 1] = "its absolute humidity decreases";
            myOptions[136, 2] = "its relative humidity remains constant";
            myOptions[136, 3] = "its absolute humidity increases";
            correct[136] = "the amount of moisture it can hold decreases";

            //INDEX = 137
            myQuestion[137] = "Q" + index.ToString() + " " + "When did France became Republic?";
            myOptions[137, 0] = "1789 AD";
            myOptions[137, 1] = "1798 AD";
            myOptions[137, 2] = "1792 AD";
            myOptions[137, 3] = "1729 AD";
            correct[137] = "1792 AD";

            //INDEX = 138
            myQuestion[138] = "Q" + index.ToString() + " " + "What is the purpose of OECD (Organisation for Economic Cooperation and Development)?";
            myOptions[138, 0] = "Sustained economic growth";
            myOptions[138, 1] = "Employment";
            myOptions[138, 2] = "Higher standards of living";
            myOptions[138, 3] = "All of the above";
            correct[138] = "All of the above";

            //INDEX = 139
            myQuestion[139] = "Q" + index.ToString() + " " + "When and where was weightlifting introduced in Olympics?";
            myOptions[139, 0] = "1986 at Athens";
            myOptions[139, 1] = "1988 at Seoul";
            myOptions[139, 2] = "1924 at St. Louis";
            myOptions[139, 3] = "1908 at London";
            correct[139] = "1986 at Athens";

            //INDEX = 140
            myQuestion[140] = "Q" + index.ToString() + " " + "What is the purpose of 'United Nations Conference on Trade and Development' (UNCTAD)?";
            myOptions[140, 0] = "Promotes International Trade with a view to accelerate economic growth of developing countries";
            myOptions[140, 1] = "Promotes International Monetary co-operation & expansion of International Trade";
            myOptions[140, 2] = "Set rules for World Trade";
            myOptions[140, 3] = "None of the above";
            correct[140] = "Promotes International Trade with a view to accelerate economic growth of developing countries";

            //INDEX = 141
            myQuestion[141] = "Q" + index.ToString() + " " + "Which country to host 2018 Junior Asian Wrestling Championship (JAWC)?";
            myOptions[141, 0] = "South Africa";
            myOptions[141, 1] = "China";
            myOptions[141, 2] = "Russia";
            myOptions[141, 3] = "India";
            correct[141] = "India";

            //INDEX = 142
            myQuestion[142] = "Q" + index.ToString() + " " + "Which team has won the 2017 UEFA Super Cup football tournament?";
            myOptions[142, 0] = "Real Madrid";
            myOptions[142, 1] = "Barcelona";
            myOptions[142, 2] = "Manchester United";
            myOptions[142, 3] = "Chelsea";
            correct[142] = "Real Madrid";

            //INDEX = 143
            myQuestion[143] = "Q" + index.ToString() + " " + "Which country won the ICC Women's World Cup final?";
            myOptions[143, 0] = "South Africa";
            myOptions[143, 1] = "New Zealand";
            myOptions[143, 2] = "India";
            myOptions[143, 3] = "England";
            correct[143] = "England";

            //INDEX = 144
            myQuestion[144] = "Q" + index.ToString() + " " + "India is in __________ position in FIFA World ranking.";
            myOptions[144, 0] = "100th";
            myOptions[144, 1] = "96th";
            myOptions[144, 2] = "82nd";
            myOptions[144, 3] = "79th";
            correct[144] = "96th";

            //INDEX = 145
            myQuestion[145] = "Q" + index.ToString() + " " + "Which film was named the Best Film at the Indian Film Festival of Melbourne 2017?";
            myOptions[145, 0] = "Airlift";
            myOptions[145, 1] = "Pink";
            myOptions[145, 2] = "MS Dhoni: The Untold Story'";
            myOptions[145, 3] = "Dangal";
            correct[145] = "Pink";

            //INDEX = 146
            myQuestion[146] = "Q" + index.ToString() + " " + "This country will host 2019 Commonwealth Weightlifting Championships?";
            myOptions[146, 0] = "India";
            myOptions[146, 1] = "Australia";
            myOptions[146, 2] = "Brazil";
            myOptions[146, 3] = "Sri Lanka";
            correct[146] = "India";

            //INDEX = 147
            myQuestion[147] = "Q" + index.ToString() + " " + "This country's quality of nationality is ranked highest in the world.";
            myOptions[147, 0] = "Germany";
            myOptions[147, 1] = "Afghanistan";
            myOptions[147, 2] = "France ";
            myOptions[147, 3] = "Iceland";
            correct[147] = "Germany";

            //INDEX = 148
            myQuestion[148] = "Q" + index.ToString() + " " + "Which country has started producing 'Civet Coffee' the most expensive coffee in the world?";
            myOptions[148, 0] = "Colombia";
            myOptions[148, 1] = "Vietnam";
            myOptions[148, 2] = "India";
            myOptions[148, 3] = "Mexico";
            correct[148] = "India";

            //INDEX = 149
            myQuestion[149] = "Q" + index.ToString() + " " + "Which country is hosting the Eastern Economic Forum (EEF-2017)?";
            myOptions[149, 0] = "India";
            myOptions[149, 1] = "United States";
            myOptions[149, 2] = "Russia";
            myOptions[149, 3] = "France";
            correct[149] = "Russia";

            //INDEX = 150
            myQuestion[150] = "Q" + index.ToString() + " " + "This country banned individuals and organisations from raising funds through initial coin offerings (ICO).";
            myOptions[150, 0] = "Bangladesh";
            myOptions[150, 1] = "China";
            myOptions[150, 2] = "France";
            myOptions[150, 3] = "Kenya";
            correct[150] = "China";

            //INDEX = 151
            myQuestion[151] = "Q" + index.ToString() + " " + "Which of the following has banned its women from wearing veils?";
            myOptions[151, 0] = "Afghanistan";
            myOptions[151, 1] = "Kyrgyzstan";
            myOptions[151, 2] = "Uzbekistan";
            myOptions[151, 3] = "Tajikistan";
            correct[151] = "Tajikistan";

            //INDEX = 152
            myQuestion[152] = "Q" + index.ToString() + " " + "The world's largest and powerful X-ray laser-Electron Laser (XFEL) was unveiled in __________.";
            myOptions[152, 0] = "Germany";
            myOptions[152, 1] = "North America";
            myOptions[152, 2] = "Africa";
            myOptions[152, 3] = "Antarctica";
            correct[152] = "Germany";

            //INDEX = 153
            myQuestion[153] = "Q" + index.ToString() + " " + "Which country imposed the world's toughest law against plastic bags recently?";
            myOptions[153, 0] = "China";
            myOptions[153, 1] = "Japan";
            myOptions[153, 2] = "Kenya";
            myOptions[153, 3] = "France";
            correct[153] = "Kenya";
        }

        public static void setQuestion(Questions IncomingClass)
        {
            retRand();
            //Setting the Question
            IncomingClass.metroLabel1.Text = myQuestion[qindex];

            //Setting the Options
            IncomingClass.metroRadioButton1.Text = myOptions[qindex,0];
            IncomingClass.metroRadioButton2.Text = myOptions[qindex,1];
            IncomingClass.metroRadioButton3.Text = myOptions[qindex,2];
            IncomingClass.metroRadioButton4.Text = myOptions[qindex,3];
        }

        public static void validateQuestion(Questions IncomingClass,string option)
        {
            if(correct[qindex] == option)
            {
                //Correct Answer
                correct_int++;
                score++;
            }
            else
            {
                wrong_int++;
            }
        }

        //END OF CLASS
    }
}
