﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizGameAppExpo2017
{
    public partial class StartForm : MetroFramework.Forms.MetroForm
    {
        MainForm OriginalForm;
        //QuestionsClass questionsClass = new QuestionsClass();

        public StartForm(MainForm IncomingForm)
        {
            OriginalForm = IncomingForm;
            InitializeComponent();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            //Start Button 
            QuestionsClass.score = 0;
            QuestionsClass.correct_int = 0;
            QuestionsClass.wrong_int = 0;

            QuestionsClass.index = 1;

            QuestionsClass.username = metroTextBox1.Text;
            //MessageBox.Show(QuestionsClass.username);
            //questionsClass.setUsername(metroTextBox1.Text);

            //Showing the Questions Page
            Questions QuestionForm = new Questions(this,OriginalForm);
            QuestionForm.Show();
            this.Close();
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            //Back Button
            OriginalForm.Show();
            this.Close();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Maximized;
        }
    }
}
