﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizGameAppExpo2017
{
    public partial class HelpForm : MetroFramework.Forms.MetroForm
    {
        MainForm OriginalForm;

        public HelpForm(MainForm IncomingForm)
        {
            OriginalForm = IncomingForm;
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            //MainForm main = new MainForm(this);
            //main.Show();
            OriginalForm.Show();
            this.Close();
        }

        private void HelpForm_Load(object sender, EventArgs e)
        {

        }
    }
}
