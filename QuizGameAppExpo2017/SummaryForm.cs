﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizGameAppExpo2017
{
    public partial class SummaryForm : MetroFramework.Forms.MetroForm
    {
        MainForm OriginalForm;

        public SummaryForm(MainForm IncomingForm)
        {
            OriginalForm = IncomingForm;
            InitializeComponent();
        }

        private void SummaryForm_Load(object sender, EventArgs e)
        {
            //Form Load

            metroLabel1.Text = "Username : " + QuestionsClass.username;
            metroLabel2.Text = "Correct : " + QuestionsClass.correct_int.ToString();
            metroLabel3.Text = "Wrong : " + QuestionsClass.wrong_int.ToString();
            metroLabel4.Text = "Total Score : " + QuestionsClass.score.ToString();

            //WindowState = FormWindowState.Maximized;
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            //Done Button
            OriginalForm.Show();
            this.Close();
        }
    }
}
