﻿namespace QuizGameAppExpo2017
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartButton = new MetroFramework.Controls.MetroTile();
            this.HelpButton = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.ActiveControl = null;
            this.StartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.StartButton.Location = new System.Drawing.Point(124, 305);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(1033, 73);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "START";
            this.StartButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.StartButton.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.StartButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.StartButton.UseSelectable = true;
            this.StartButton.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // HelpButton
            // 
            this.HelpButton.ActiveControl = null;
            this.HelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.HelpButton.Location = new System.Drawing.Point(124, 384);
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Size = new System.Drawing.Size(1033, 73);
            this.HelpButton.TabIndex = 1;
            this.HelpButton.Text = "HELP";
            this.HelpButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.HelpButton.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.HelpButton.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.HelpButton.UseSelectable = true;
            this.HelpButton.Click += new System.EventHandler(this.metroTile2_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.HelpButton);
            this.Controls.Add(this.StartButton);
            this.Name = "MainForm";
            this.Text = "Expo EFIA 2017";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile StartButton;
        private MetroFramework.Controls.MetroTile HelpButton;
    }
}

