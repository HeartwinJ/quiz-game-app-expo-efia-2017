﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizGameAppExpo2017
{
    public partial class MainForm : MetroFramework.Forms.MetroForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            //Start Button
            StartForm start = new StartForm(this);
            start.Show();
            this.Hide();
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            //Help Button
            HelpForm help = new HelpForm(this);
            help.Show();
            this.Hide();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //WindowState = FormWindowState.Maximized;
        }
    }
}
